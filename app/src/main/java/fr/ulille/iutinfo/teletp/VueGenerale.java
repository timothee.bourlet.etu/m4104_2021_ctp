package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String DISTANCIEL;
    private String salle ;
    private String poste;
    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL=getResources().getStringArray(R.array.list_salles)[0];
        this.salle = DISTANCIEL;
        this.poste="";
        // TODO Q2.c
        SuiviViewModel model = new ViewModelProvider(this).get(SuiviViewModel.class);

        // TODO Q4
        Spinner spSalle = view.findViewById(R.id.spSalle);
        Spinner spPoste = view.findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> adapSalle =ArrayAdapter.createFromResource(
                getActivity().getApplicationContext(),
                R.array.list_salles,
                android.R.layout.simple_spinner_item);

        ArrayAdapter<CharSequence> adapPoste =ArrayAdapter.createFromResource(
                getActivity().getApplicationContext(),
                R.array.list_postes,
                android.R.layout.simple_spinner_item);

        spSalle.setAdapter(adapSalle);
        spPoste.setAdapter(adapPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            EditText login = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(login.getText().toString());
        });


        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        if (spSalle.getSelectedItem().equals("Distanciel")){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        }
        else{
            model.setLocalisation(spSalle.getSelectedItem().toString()+" : "+spPoste.getSelectedItem().toString());
        }
    }
    // TODO Q9
}